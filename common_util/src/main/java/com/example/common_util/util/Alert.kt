package com.example.common_util.utils

import androidx.appcompat.app.AlertDialog
import android.content.Context
import android.content.DialogInterface

object Alert {

    /* Global Alertdialog for application*/
    fun showDialog(context: Context, message: String) {
        val alertDialog = AlertDialog.Builder(context)
        //        alertDialog.setTitle(mContext.getResources().getString(R.string.app_name));
        alertDialog.setMessage(message)
        alertDialog.setCancelable(false)
        alertDialog.setNegativeButton("OK") { dialog, which -> dialog.dismiss() }
        val alert = alertDialog.create()

        // show it
        alert.show()
    }

    fun showDialog(context: Context, message: Int) {
        val alertDialog = AlertDialog.Builder(context)
        //        alertDialog.setTitle(mContext.getResources().getString(R.string.app_name));
        alertDialog.setMessage(message)
        alertDialog.setCancelable(false)
        alertDialog.setNegativeButton("OK") { dialog, which -> dialog.dismiss() }
        val alert = alertDialog.create()

        // show it
        alert.show()
    }

    /* Global Alertdialog for application*/
    fun showDialog(
        context: Context,
        message: String,
        onClickListenerText: String,
        onClickListener: DialogInterface.OnClickListener?
    ): AlertDialog {
        val alertDialog = AlertDialog.Builder(context)
        // alertDialog.setTitle(mContext.getResources().getString(R.string.app_name));
        alertDialog.setMessage(message)
        alertDialog.setCancelable(false)
        alertDialog.setNegativeButton(onClickListenerText) { dialog, which ->
            dialog.dismiss()
            onClickListener?.onClick(dialog, which)
        }
        val alert = alertDialog.create()

        // show it
        alert.show()
        return alert
    }

    /* Global Alertdialog for application*/
    fun showDialog(
        context: Context,
        message: String,
        onClickListenerText: String,
        onCancelListenerText: String,
        onClickListener: DialogInterface.OnClickListener?,
        onCancelListener: DialogInterface.OnClickListener?
    ): AlertDialog? {
        val alertDialog = AlertDialog.Builder(context)
        //        alertDialog.setTitle(mContext.getResources().getString(R.string.app_name));
        alertDialog.setMessage(message)
        alertDialog.setCancelable(false)
        alertDialog.setPositiveButton(onClickListenerText) { dialog, which ->
            dialog.dismiss()
            onClickListener?.onClick(dialog, which)
        }
        alertDialog.setNegativeButton(onCancelListenerText) { dialog, which ->
            dialog.dismiss()
            onCancelListener?.onClick(dialog, which)
        }
        val alert = alertDialog.create()

        // show it
        try {
            alert.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return alert
    }

    /* Global Alertdialog for application*/
    fun showDialog(
        context: Context, theme : Int ,
        message: String,
        onClickListenerText: String,
        onCancelListenerText: String,
        onClickListener: DialogInterface.OnClickListener?,
        onCancelListener: DialogInterface.OnClickListener?
    ): AlertDialog? {
        val alertDialog = AlertDialog.Builder(context,theme)
        //        alertDialog.setTitle(mContext.getResources().getString(R.string.app_name));
        alertDialog.setMessage(message)
        alertDialog.setCancelable(false)
        alertDialog.setPositiveButton(onClickListenerText) { dialog, which ->
            dialog.dismiss()
            onClickListener?.onClick(dialog, which)
        }
        alertDialog.setNegativeButton(onCancelListenerText) { dialog, which ->
            dialog.dismiss()
            onCancelListener?.onClick(dialog, which)
        }
        val alert = alertDialog.create()

        // show it
        try {
            alert.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return alert
    }

}
