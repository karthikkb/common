package com.example.common_util.util.network

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import java.net.InetAddress
import java.net.UnknownHostException
import java.util.concurrent.*

object NetworkHelper {

    private val TAG = "NetworkHelper"


        fun isNetworkConnected(context : Context): Boolean {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            var result = false
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val networkCapabilities = connectivityManager.activeNetwork ?: return false
                val actNw =
                    connectivityManager.getNetworkCapabilities(networkCapabilities) ?: return false
                result = when {
                    actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                    actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                    actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                    else -> false
                }
            } else {
                @Suppress("DEPRECATION")
                connectivityManager.run {
                    connectivityManager.activeNetworkInfo?.run {
                        result = when (type) {
                            ConnectivityManager.TYPE_WIFI -> true
                            ConnectivityManager.TYPE_MOBILE -> true
                            ConnectivityManager.TYPE_ETHERNET -> true
                            else -> false
                        }

                    }
                }
            }

            return result
        }


        fun isInternetAvailable(context: Context): Boolean {
            if (isNetworkConnected(context)) {
                var inetAddress: InetAddress? = null
                try {
                    val future = Executors.newSingleThreadExecutor().submit(Callable<InetAddress> {
                        try {
                            return@Callable InetAddress.getByName("google.com")
                        } catch (e: UnknownHostException) {
                            return@Callable null
                        }
                    })
                    inetAddress = future.get(2000, TimeUnit.MILLISECONDS)
                    future.cancel(true)
                } catch (e: InterruptedException) {
                } catch (e: ExecutionException) {
                } catch (e: TimeoutException) {
                }

                return (inetAddress != null)
            } else
                return false
        }

        fun getNetworkType(context : Context): Int {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetworkInfo = connectivityManager?.activeNetworkInfo
            return activeNetworkInfo?.type ?: 0

        }
    }

