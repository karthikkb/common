package com.example.common_util.utils

import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView

import androidx.core.content.ContextCompat
import com.example.common_util.R

object Toaster {

    fun show(context: Context, text: String) {
        val toast = Toast.makeText(context, text, android.widget.Toast.LENGTH_SHORT)
        toast.view.background.setColorFilter(
            ContextCompat.getColor(context, R.color.black ), PorterDuff.Mode.SRC_IN
        )
        val textView = toast.view.findViewById<View>(android.R.id.message) as TextView
        textView.setTextColor(ContextCompat.getColor(context, R.color.white))
        toast.setGravity(Gravity.CENTER, 0, 0)
        toast.show()
    }

    fun show(context: Context, text: Int?) {
        val toast = Toast.makeText(context, text!!, android.widget.Toast.LENGTH_SHORT)
        toast.view.background.setColorFilter(
            ContextCompat.getColor(context, R.color.black ), PorterDuff.Mode.SRC_IN
        )
        val textView = toast.view.findViewById<View>(android.R.id.message) as TextView
        textView.setTextColor(ContextCompat.getColor(context, R.color.white))
        toast.setGravity(Gravity.CENTER, 0, 0)
        toast.show()
    }

    fun toastMessageImage(context: Context,message:Int,img : Int) {
        val toast = Toast.makeText(context, "Show Image In Toast", Toast.LENGTH_SHORT)
        toast.setGravity(Gravity.CENTER, 0, 0)
        val toastContentView = toast.view as LinearLayout
        val imageView = ImageView(context)
        imageView.setImageResource(img)
        toastContentView.addView(imageView, 0)
        toast.show()
    }


}
