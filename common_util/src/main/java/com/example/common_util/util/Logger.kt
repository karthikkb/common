package com.example.common_util.utils

import timber.log.Timber

object Logger {

    fun d(tag: String, s: String, params: Any) {
        Timber.tag(tag).d(s, params)
    }

    fun d(tag: String, s : String) {
        Timber.tag(tag).d( s, "")
    }

    fun d(tag: String, throwable: Throwable, s: String, params: Any) {
        Timber.tag(tag).d(throwable, s, params)
    }

    fun i(tag: String, s: String, params: Any) {
        Timber.tag(tag).i(s, params)
    }

    fun i(tag: String, throwable: Throwable, s: String, params: Any) {
        Timber.tag(tag).i(throwable, s, params)
    }

    fun w(tag: String, s: String, params: Any) {
        Timber.tag(tag).w(s, params)
    }
    fun w(tag: String, s: String) {
        Timber.tag(tag).w(s, "")
    }

    fun w(tag: String, throwable: Throwable, s: String, params: Any) {
        Timber.tag(tag).w(throwable, s, params)
    }

    fun e(tag: String, s: String) {
        Timber.tag(tag).e(s, "")
    }

    fun e(tag: String, s: String, params: Any) {
        Timber.tag(tag).e(s, params)
    }

    fun e(tag: String, throwable: Throwable, s: String, params: Any) {
        Timber.tag(tag).e(throwable, s, params)
    }
}
