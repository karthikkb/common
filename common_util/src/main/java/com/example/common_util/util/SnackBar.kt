package com.example.common_util.utils

import android.view.View
import android.widget.TextView

import com.google.android.material.snackbar.Snackbar

object SnackBar {

    fun show(view: View, text: String) {
        Snackbar.make(view, text, Snackbar.LENGTH_LONG).show()
    }

    fun customShow(view: View, textDisplay: String, textAction: String,textColor : Int, textActionButtonColor : Int,callBack : SnackBarCallBack) {
        val snackbar = Snackbar.make(view, textDisplay, Snackbar.LENGTH_LONG)
                        .setAction(textAction, object :View.OnClickListener{
                            override fun onClick(v: View?) {
                                callBack.retry()
                            }

                        })
        snackbar.setActionTextColor(textActionButtonColor)
        val sbView = snackbar.view
        val textView =
            sbView.findViewById<View>(com.google.android.material.R.id.snackbar_text) as TextView
        textView.setTextColor(textColor)
        snackbar.show()
    }

    interface SnackBarCallBack{
        fun retry()
    }

}
