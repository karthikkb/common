package com.example.common_util.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel


abstract class BaseViewModel : ViewModel() {

    val isKeyboardHide: MutableLiveData<Boolean> = MutableLiveData()
    val toastMessageId: MutableLiveData<Int> = MutableLiveData()
    val toastMessageString: MutableLiveData<String> = MutableLiveData()
    val alertMessageId: MutableLiveData<Int> = MutableLiveData()
    val alertMessageString: MutableLiveData<String> = MutableLiveData()


    init {
        setIsKeyboardHide(false)
    }

    override fun onCleared() {
        super.onCleared()
    }


    fun setIsKeyboardHide(isKeyboardHide: Boolean) {
        this.isKeyboardHide.value = isKeyboardHide
    }

    fun toast(message: Int) {
        this.toastMessageId.value = message
    }

    fun toast(message: String) {
        this.toastMessageString.value = message
    }

    fun alert(message: Int) {
        this.alertMessageId.value = message
    }

    fun alert(message: String) {
        this.alertMessageString.value = message
    }


}
