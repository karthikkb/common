package com.example.common_util.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.common_util.utils.Logger

abstract class BaseFragment<T : ViewDataBinding, V : BaseViewModel> : Fragment() {

    var baseActivity: BaseActivity<*, *>? = null
        private set
    private var mRootView: View? = null
    protected lateinit var mViewDataBinding: T
    protected lateinit var mViewModel: V

    /**
     * Override for set binding variable
     *
     * @return variable id
     */
    abstract val bindingVariable: Int

    /**
     * @return layout resource id
     */
    @get:LayoutRes
    abstract val layoutId: Int

    /**
     * Override for set view model
     *
     * @return view model instance
     */
    abstract val viewModel: V

    val isNetworkConnected: Boolean
        get() = baseActivity != null && baseActivity!!.isNetworkConnected

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseActivity<*, *>) {
            this.baseActivity = context
            //            activity.onFragmentAttached();
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        //        performDependencyInjection();
        super.onCreate(savedInstanceState)
        mViewModel = viewModel
        setHasOptionsMenu(false)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mViewDataBinding = DataBindingUtil.inflate(inflater, layoutId, container, false)
        mRootView = mViewDataBinding.root
        return mRootView
    }

    override fun onDetach() {
        baseActivity = null
        super.onDetach()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewDataBinding.setVariable(bindingVariable, mViewModel)
        mViewDataBinding.lifecycleOwner = this
        mViewDataBinding.executePendingBindings()
        initObservables()
    }

    protected fun initObservables() {

        mViewModel.isKeyboardHide.observe(viewLifecycleOwner, Observer { status ->
            Logger.e("isLoading.observe", "", "" + status!!)
            if (status) {
                baseActivity!!.hideKeyboard()
            }
        })


        mViewModel.toastMessageId.observe(viewLifecycleOwner, Observer { integer -> baseActivity!!.toastMessage(integer) })

        mViewModel.toastMessageString.observe(viewLifecycleOwner, Observer { s -> baseActivity!!.toastMessage(s) })

        mViewModel.alertMessageId.observe(viewLifecycleOwner, Observer { integer -> baseActivity!!.alertMessage(integer) })

        mViewModel.alertMessageString.observe(viewLifecycleOwner, Observer { s -> baseActivity!!.alertMessage(s) })

    }

    fun openActivityOnTokenExpire() {
        if (baseActivity != null) {
            baseActivity!!.openActivityOnTokenExpire()
        }
    }


    interface Callback {

        fun onFragmentAttached()

        fun onFragmentDetached(tag: String)
    }

}
